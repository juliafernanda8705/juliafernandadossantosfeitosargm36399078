package br.edu.up.service;

import java.util.List;

import br.edu.up.Controllers.CSVWriter;
import br.edu.up.Models.Aluno;

public class ProcessadorDeAlunos {
    public String ArquivosCaminho = "C:\\Users\\autologon\\Documents\\provajava\\provajava\\src\\br\\edu\\up\\Arquivo\\";

    public void processarDados(List<Aluno> listaAlunos) {
        int quantidadeAlunos = listaAlunos.size();
        int aprovados = 0;
        int reprovados = 0;
        float menorNota = Float.MAX_VALUE;
        float maiorNota = Float.MIN_VALUE;
        float somaDasNotas = 0;

        for (Aluno aluno : listaAlunos) {
            float nota = aluno.getNota();
            somaDasNotas += nota;
            if (nota >= 6.0) {
                aprovados++;
            } else {
                reprovados++;
            }
            if (nota < menorNota) {
                menorNota = nota;
            }
            if (nota > maiorNota) {
                maiorNota = nota;
            }
        }

        float mediaGeral = somaDasNotas / quantidadeAlunos;

        CSVWriter writer = new CSVWriter();
        writer.escreverResumo(quantidadeAlunos, aprovados, reprovados, menorNota, maiorNota, mediaGeral);
    }
}