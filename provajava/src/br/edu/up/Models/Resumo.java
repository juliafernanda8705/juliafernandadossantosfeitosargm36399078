package br.edu.up.Models;

public class Resumo {
    public int QuantidadeDeAlunos;
    public int aprovados;
    public int reprovados;
    public float menorNota;
    public float maiorNota;
    public float mediaGeral;

    public int getQuantidadeDeAlunos() {
        return QuantidadeDeAlunos;
    }

    public void setQuantidadeDeAlunos(int quantidadeDeAlunos) {
        QuantidadeDeAlunos = quantidadeDeAlunos;
    }

    public int getAprovados() {
        return aprovados;
    }

    public void setAprovados(int aprovados) {
        this.aprovados = aprovados;
    }

    public int getReprovados() {
        return reprovados;
    }

    public void setReprovados(int reprovados) {
        this.reprovados = reprovados;
    }

    public float getMenorNota() {
        return menorNota;
    }

    public void setMenorNota(float menorNota) {
        this.menorNota = menorNota;
    }

    public float getMaiorNota() {
        return maiorNota;
    }

    public void setMaiorNota(float maiorNota) {
        this.maiorNota = maiorNota;
    }

    public float getMediaGeral() {
        return mediaGeral;
    }

    public void setMediaGeral(float mediaGeral) {
        this.mediaGeral = mediaGeral;
    }

    public void add(String resumo) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'add'");
    }

}
