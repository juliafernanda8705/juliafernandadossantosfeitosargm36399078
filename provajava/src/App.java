import java.util.List;

import br.edu.up.DAO.AlunoDAO;
import br.edu.up.Models.Aluno;
import br.edu.up.service.ProcessadorDeAlunos;

public class App {

    public static void main(String[] args) {
                AlunoDAO alunoDAO = new AlunoDAO();
        List<Aluno> listaAlunos = alunoDAO.lerAlunos();

        ProcessadorDeAlunos processador = new ProcessadorDeAlunos();
        processador.processarDados(listaAlunos);
    }
}
