package br.edu.up.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CSVWriter {

    private static final String ARQUIVO_RESUMO = "resumo.csv";

    public void escreverResumo(int quantidadeAlunos, int aprovados, int reprovados,
                               float menorNota, float maiorNota, float mediaGeral) {
        try (FileWriter fileWriter = new FileWriter(ARQUIVO_RESUMO);
             PrintWriter printWriter = new PrintWriter(fileWriter)) {

            printWriter.println("Quantidade de Alunos;Aprovados;Reprovados;Menor Nota;Maior Nota;Média Geral");

            printWriter.printf("%d;%d;%d;%.2f;%.2f;%.2f%n",
                    quantidadeAlunos, aprovados, reprovados, menorNota, maiorNota, mediaGeral);

            System.out.println("Dados gravados com sucesso em " + ARQUIVO_RESUMO);

        } catch (IOException e) {
            System.err.println("Erro ao escrever no arquivo " + ARQUIVO_RESUMO + ": " + e.getMessage());
        }
    }
}