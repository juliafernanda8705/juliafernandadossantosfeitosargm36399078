package br.edu.up.DAO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.edu.up.Models.Aluno;

public class AlunoDAO {
    public String ArquivosCaminho = "C:\\Users\\autologon\\Documents\\provajava\\provajava\\src\\br\\edu\\up\\Arquivo\\";
    private static final String ARQUIVO_ALUNOS = "alunos.csv";

    public List<Aluno> lerAlunos() {
        List<Aluno> listaAlunos = new ArrayList<>();

        try {
            File arquivo = new File(ARQUIVO_ALUNOS);
            Scanner scanner = new Scanner(arquivo);

            while (scanner.hasNextLine()) {
                String linha = scanner.nextLine();
                String[] dados = linha.split(";");

                int matricula = Integer.parseInt(dados[0].trim());
                String nome = dados[1].trim();
                float nota = Float.parseFloat(dados[2].trim());

                Aluno aluno = new Aluno();
                listaAlunos.add(aluno);
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            System.err.println("Erro ao ler o arquivo " + ARQUIVO_ALUNOS + ": " + e.getMessage());
        }

        return listaAlunos;
    }
}